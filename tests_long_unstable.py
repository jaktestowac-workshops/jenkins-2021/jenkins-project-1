import unittest
import random
import time


class SampleUnstableTests(unittest.TestCase):
    def setUp(self):
        print('Setting up tests...')

        # simulate some long operations by sleep:
        time.sleep(random.randint(1, 2))

    def tearDown(self):
        print('Tearing Down up tests...')

    def test_unstable(self):
        expected_value = 1
        actual_value = random.randint(0, 1)

        # simulate some long operations by sleep:
        time.sleep(random.randint(1, 3))

        self.assertEqual(expected_value, actual_value)

    def test_another_unstable(self):
        expected_value = 1
        actual_value = random.randint(0, 1)
        
        # simulate some long operations by sleep:
        time.sleep(random.randint(1, 3))

        self.assertEqual(expected_value, actual_value)

    def test_very_unstable(self):
        expected_value = 1
        actual_value = random.randint(0, 2)
        
        # simulate some long operations by sleep:
        time.sleep(random.randint(1, 3))

        self.assertEqual(expected_value, actual_value)


if __name__ == '__main__':
    unittest.main()